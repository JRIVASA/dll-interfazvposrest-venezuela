﻿Imports System.IO
Imports System.Net
Imports Newtonsoft.Json

Public Class JSON_resul

    Public CodRespuesta As String
    Public mensajeRespuesta As String
    Public nombreVoucher As String
    Public numSeq As String
    Public numeroTarjeta As String
    Public cedula As String
    Public montoTransaccion As String
    Public montoAvance As String
    Public montoServicios As String
    Public montoServiciosAprobado As String
    Public tipoCuenta As String
    Public tipoTarjeta As String
    Public fechaExpiracion As String
    Public existeCopiaVoucher As String
    Public fechaTransaccion As String
    Public horaTransaccion As String
    Public terminalVirtual As String
    Public tipoTransaccion As String
    Public numeroAutorizacion As String
    Public codigoAfiliacion As String
    Public tid As String
    Public numeroReferencia As String
    Public nombreAutorizador As String
    Public codigoAdquiriente As String
    Public numeroLote As String
    Public tipoProducto As String
    Public bancoEmisorCheque As String
    Public numeroCuenta As String
    Public numeroCheque As String
    Public archivoCierre As String

End Class

Public Class JSON_Resul_Cards

    Public CodRespuesta As String
    Public mensajeRespuesta As String
    Public nombreVoucher As String
    Public numSeq As String
    Public numeroTarjeta As String
    Public fechaExpiracion As String
    Public cedula As String
    Public montoTransaccion As String
    Public existeCopiaVoucher As String
    Public fechaTransaccion As String
    Public horaTransaccion As String
    Public terminalVirtual As String
    Public tipoTransaccion As String
    Public numeroAutorizacion As String
    Public codigoAfiliacion As String
    Public tid As String
    Public numeroReferencia As String
    Public nombreAutorizador As String
    Public codigoAdquiriente As String
    Public numeroLote As String
    Public tipoProducto As String
    Public numeroTarjetaEnmascarado As String
    Public saldoDisponible As String
    Public archivoCierre As String
    Public flagImpresion As String

End Class

Public Class ClassInterfaz

    Private DebugMode As Boolean

    Public Codigo As String
    Public Mensaje As String
    Public Voucher As String
    Public Referencia As String
    Public NumTarjeta As String
    Public NumCedula As String
    Public MontoCompra As String
    Public MontoAvanceEfectivo As String
    Public MontoDeServicios As String
    Public MontoDeServiciosAprobado As String
    Public TipoCta As String
    Public TipoDeTarjeta As String
    Public FechaExp As String
    Public HayCopiaVoucher As String
    Public FechaTrans As String
    Public HoraTrans As String
    Public TerminalV As String
    Public TipoTrans As String
    Public NumAutorizacion As String
    Public CodigoDeAfiliacion As String
    Public TerminalID As String
    Public NumRef As String
    Public NombreBancoAutorizador As String
    Public CodigoBancoAdquiriente As String
    Public NumLote As String
    Public TipoProd As String
    Public BancoCheque As String
    Public NumCuenta As String
    Public NumCheque As String
    Public ArchivoCierre As String
    Public NumeroDeTarjetaEnmascarado As String
    Public SaldoDisponible As String

    'Public JsonRe As JSON_resul

    'Public Property CodRespuest() As String
    '    Get
    '        Return codR
    '    End Get
    '    Set(ByVal value As String)
    '        codR = value
    '    End Set
    'End Property

    'Public Sub IniciarServicioVPos()

    '    Dim link = "http://localhost:8085/vpos/metodo"
    '    Dim request As WebRequest = System.Net.WebRequest.Create(link)
    '    request.Method = "POST"
    '    request.ContentType = "application/json;charset=utf-8"

    'End Sub

    'Codigo = resp.CodRespuesta
    'Mensaje = resp.mensajeRespuesta
    'Voucher = resp.nombreVoucher
    'Referencia = resp.numeroReferencia
    'NumTarjeta = resp.numeroTarjeta
    'NumCedula = resp.cedula
    'Monto = resp.montoTransaccion
    'MontoAvanceEfectivo = resp.montoAvance
    'MontoDeServicios = resp.montoServicios
    'MontoDeServiciosAprobado = resp.montoServiciosAprobado
    'TipoCta = resp.tipoCuenta
    'TipoDeTarjeta = resp.tipoTarjeta
    'FechaExp = resp.fechaExpiracion
    'HayCopiaVoucher = resp.existeCopiaVoucher
    'FechaTrans = resp.fechaTransaccion
    'HoraTrans = resp.horaTransaccion
    'TerminalV = resp.terminalVirtual
    'TipoTrans = resp.tipoTransaccion
    'NumAutorizacion = resp.numeroAutorizacion
    'CodigoDeAfiliacion = resp.codigoAfiliacion
    'TerminalID = resp.tid
    'NumRef = resp.numeroReferencia
    'NombreBancoAutorizador = resp.nombreAutorizador
    'CodigoBancoAdquiriente = resp.codigoAdquiriente
    'NumLote = resp.numeroLote
    'TipoProd = resp.tipoProducto
    'BancoCheque = resp.bancoEmisorCheque
    'NumCuenta = resp.numeroCuenta
    'NumCheque = resp.numeroCheque
    'ArchivoCierre = resp.archivoCierre

    Public Property Prop_DebugMode() As Boolean
        Get
            Return DebugMode
        End Get
        Set(ByVal value As Boolean)
            DebugMode = value
        End Set
    End Property

    Public Function CompraConTarjetas(Monto As Double, Optional Cedula As String = "") As Boolean
        Try
            LimpiarVariables()
            Dim request As WebRequest = System.Net.WebRequest.Create(Link)
            request.Method = "POST"
            request.ContentType = "application/json;charset=utf-8"
            Dim data As New String("{ ""accion"":""tarjeta"", ""montoTransaccion"":""" & CStr(Monto) & """, ""cedula"":""" & Cedula & """ }")

            If DebugMode Then
                MsgBox(data, MsgBoxStyle.Information, "DebugMode - Request")
            End If

            Dim encoding As New System.Text.UTF8Encoding()
            Dim bytes As Byte() = encoding.GetBytes(data)
            Using requestStream As Stream = request.GetRequestStream()
                'Send the data.
                requestStream.Write(bytes, 0, bytes.Length)
            End Using
            Dim response As WebResponse = request.GetResponse
            If CType(response, HttpWebResponse).StatusCode = HttpStatusCode.OK Then
                Dim dataStream As New StreamReader(response.GetResponseStream)
                Dim result As String = dataStream.ReadToEnd

                Dim resp As JSON_resul
                resp = JsonConvert.DeserializeObject(Of JSON_resul)(result)

                Codigo = resp.CodRespuesta
                Mensaje = resp.mensajeRespuesta
                Voucher = resp.nombreVoucher
                Referencia = resp.numeroReferencia
                NumTarjeta = resp.numeroTarjeta
                NumCedula = resp.cedula
                MontoCompra = resp.montoTransaccion
                'MontoAvanceEfectivo = resp.montoAvance
                'MontoDeServicios = resp.montoServicios
                'MontoDeServiciosAprobado = resp.montoServiciosAprobado
                TipoCta = resp.tipoCuenta
                TipoDeTarjeta = resp.tipoTarjeta
                FechaExp = resp.fechaExpiracion
                HayCopiaVoucher = resp.existeCopiaVoucher
                FechaTrans = resp.fechaTransaccion
                HoraTrans = resp.horaTransaccion
                TerminalV = resp.terminalVirtual
                TipoTrans = resp.tipoTransaccion
                NumAutorizacion = resp.numeroAutorizacion
                CodigoDeAfiliacion = resp.codigoAfiliacion
                TerminalID = resp.tid
                NumRef = resp.numeroReferencia
                NombreBancoAutorizador = resp.nombreAutorizador
                CodigoBancoAdquiriente = resp.codigoAdquiriente
                NumLote = resp.numeroLote
                TipoProd = resp.tipoProducto
                'BancoCheque = resp.bancoEmisorCheque
                NumCuenta = resp.numeroCuenta
                'NumCheque = resp.numeroCheque
                'ArchivoCierre = resp.archivoCierre

                If resp.CodRespuesta = "00" Then
                    Return True
                Else
                    Return False
                End If

            End If
        Catch ex As Exception
            'MsgBox(Err.Description, , "Compra Con Tarjetas VPosRest")
            Codigo = "-1"
            Mensaje = "Error al intentar establecer comunicación con el servicio. Información Adicional:" + vbNewLine +
            Err.Description + " (" + Err.Number.ToString() + ")"
            Return False
        End Try
    End Function

    Public Function CompraPorTecladoAbierto(Monto As Double, Optional Cedula As String = "") As Boolean
        Try
            Dim request As WebRequest = System.Net.WebRequest.Create(Link)
            request.Method = "POST"
            request.ContentType = "application/json;charset=utf-8"
            Dim data As New String("{ ""accion"":""tecladoAbierto"", ""montoTransaccion"":""" & Monto & """, ""cedula"":""" & Cedula & """ }")

            If DebugMode Then
                MsgBox(data, MsgBoxStyle.Information, "DebugMode - Request")
            End If

            Dim encoding As New System.Text.UTF8Encoding()
            Dim bytes As Byte() = encoding.GetBytes(data)
            Using requestStream As Stream = request.GetRequestStream()
                'Send the data.
                requestStream.Write(bytes, 0, bytes.Length)
            End Using
            Dim response As WebResponse = request.GetResponse
            If CType(response, HttpWebResponse).StatusCode = HttpStatusCode.OK Then
                Dim dataStream As New StreamReader(response.GetResponseStream)
                Dim result As String = dataStream.ReadToEnd

                Dim resp As JSON_resul
                resp = JsonConvert.DeserializeObject(Of JSON_resul)(result)

                If resp.CodRespuesta = "00" Then
                    Return True
                Else
                    Return False
                End If
            End If
        Catch ex As Exception
            'MsgBox(Err.Description, , "Compra Por Teclado Abierto2 VPosRest")
            Codigo = "-1"
            Mensaje = "Error al intentar establecer comunicación con el servicio. Información Adicional:" + vbNewLine +
            Err.Description + " (" + Err.Number.ToString() + ")"
            Return False
        End Try
    End Function

    Public Function Anulacion(Optional Numero As String = "") As Boolean
        Try
            LimpiarVariables()
            Dim link = "http://localhost:8085/vpos/metodo"
            Dim request As WebRequest = System.Net.WebRequest.Create(link)
            request.Method = "POST"
            request.ContentType = "application/json;charset=utf-8"
            Dim data As New String("{ ""accion"":""anulacion"", ""numSeq"":""" & Numero & """ }")

            If DebugMode Then
                MsgBox(data, MsgBoxStyle.Information, "DebugMode - Request")
            End If

            Dim encoding As New System.Text.UTF8Encoding()
            Dim bytes As Byte() = encoding.GetBytes(data)
            Using requestStream As Stream = request.GetRequestStream()
                'Send the data.
                requestStream.Write(bytes, 0, bytes.Length)
            End Using
            Dim response As WebResponse = request.GetResponse
            If CType(response, HttpWebResponse).StatusCode = HttpStatusCode.OK Then
                Dim dataStream As New StreamReader(response.GetResponseStream)
                Dim result As String = dataStream.ReadToEnd

                Dim resp As JSON_resul
                resp = JsonConvert.DeserializeObject(Of JSON_resul)(result)

                Codigo = resp.CodRespuesta
                Mensaje = resp.mensajeRespuesta
                Voucher = resp.nombreVoucher
                Referencia = resp.numeroReferencia
                NumTarjeta = resp.numeroTarjeta
                NumCedula = resp.cedula
                MontoCompra = resp.montoTransaccion
                'MontoAvanceEfectivo = resp.montoAvance
                'MontoDeServicios = resp.montoServicios
                'MontoDeServiciosAprobado = resp.montoServiciosAprobado
                TipoCta = resp.tipoCuenta
                TipoDeTarjeta = resp.tipoTarjeta
                FechaExp = resp.fechaExpiracion
                HayCopiaVoucher = resp.existeCopiaVoucher
                FechaTrans = resp.fechaTransaccion
                HoraTrans = resp.horaTransaccion
                TerminalV = resp.terminalVirtual
                TipoTrans = resp.tipoTransaccion
                NumAutorizacion = resp.numeroAutorizacion
                CodigoDeAfiliacion = resp.codigoAfiliacion
                TerminalID = resp.tid
                NumRef = resp.numeroReferencia
                NombreBancoAutorizador = resp.nombreAutorizador
                CodigoBancoAdquiriente = resp.codigoAdquiriente
                NumLote = resp.numeroLote
                TipoProd = resp.tipoProducto
                'BancoCheque = resp.bancoEmisorCheque
                NumCuenta = resp.numeroCuenta
                'NumCheque = resp.numeroCheque
                'ArchivoCierre = resp.archivoCierre

                If resp.CodRespuesta = "00" Then
                    Return True
                Else
                    Return False
                End If
            End If

        Catch ex As Exception
            'MsgBox(Err.Description, , "Anulación VPosRest")
            Codigo = "-1"
            Mensaje = "Error al intentar establecer comunicación con el servicio. Información Adicional:" + vbNewLine +
            Err.Description + " (" + Err.Number.ToString() + ")"
            Return False
        End Try
    End Function

    Public Function PreCierre() As Boolean
        Try
            LimpiarVariables()
            Dim request As WebRequest = System.Net.WebRequest.Create(Link)
            request.Method = "POST"
            request.ContentType = "application/json;charset=utf-8"
            Dim data As New String("{ ""accion"":""precierre""}")

            If DebugMode Then
                MsgBox(data, MsgBoxStyle.Information, "DebugMode - Request")
            End If

            Dim encoding As New System.Text.UTF8Encoding()
            Dim bytes As Byte() = encoding.GetBytes(data)
            Using requestStream As Stream = request.GetRequestStream()
                'Send the data.
                requestStream.Write(bytes, 0, bytes.Length)
            End Using

            Dim response As WebResponse = request.GetResponse

            If CType(response, HttpWebResponse).StatusCode = HttpStatusCode.OK Then
                Dim dataStream As New StreamReader(response.GetResponseStream)
                Dim result As String = dataStream.ReadToEnd

                Dim resp As JSON_resul
                resp = JsonConvert.DeserializeObject(Of JSON_resul)(result)

                Codigo = resp.CodRespuesta
                Mensaje = resp.mensajeRespuesta
                Voucher = resp.nombreVoucher

                If resp.CodRespuesta = "00" Then
                    Return True
                Else
                    Return False
                End If

            End If

        Catch ex As Exception
            'MsgBox(Err.Description, , "PreCierre VPosRest")
            Codigo = "-1"
            Mensaje = "Error al intentar establecer comunicación con el servicio. Información Adicional:" + vbNewLine +
            Err.Description + " (" + Err.Number.ToString() + ")"
            Return False
        End Try
    End Function

    Public Function UltimaTransaccionProcesada() As String
        Try
            LimpiarVariables()
            Dim request As WebRequest = System.Net.WebRequest.Create(Link)
            request.Method = "POST"
            request.ContentType = "application/json;charset=utf-8"
            Dim data As New String("{ ""accion"":""imprimeUltimoVoucherP""}")

            If DebugMode Then
                MsgBox(data, MsgBoxStyle.Information, "DebugMode - Request")
            End If

            Dim encoding As New System.Text.UTF8Encoding()
            Dim bytes As Byte() = encoding.GetBytes(data)
            Using requestStream As Stream = request.GetRequestStream()
                'Send the data.
                requestStream.Write(bytes, 0, bytes.Length)
            End Using
            Dim response As WebResponse = request.GetResponse
            If CType(response, HttpWebResponse).StatusCode = HttpStatusCode.OK Then
                Dim dataStream As New StreamReader(response.GetResponseStream)
                Dim result As String = dataStream.ReadToEnd

                Dim resp As JSON_resul
                resp = JsonConvert.DeserializeObject(Of JSON_resul)(result)

                If resp.CodRespuesta = "00" Then
                    UltimaTransaccionProcesada = resp.nombreVoucher
                Else
                    UltimaTransaccionProcesada = ""
                End If
            End If
        Catch ex As Exception
            'MsgBox(Err.Description, , "Ultima Transaccion Procesada VPosRest")
            Codigo = "-1"
            Mensaje = "Error al intentar establecer comunicación con el servicio. Información Adicional:" + vbNewLine +
            Err.Description + " (" + Err.Number.ToString() + ")"
            Return String.Empty
        End Try
    End Function

    Public Function UltimaTransaccionAprobada() As String
        Try
            LimpiarVariables()
            Dim request As WebRequest = System.Net.WebRequest.Create(Link)
            request.Method = "POST"
            request.ContentType = "application/json;charset=utf-8"
            Dim data As New String("{ ""accion"":""imprimeUltimoVoucher""}")

            If DebugMode Then
                MsgBox(data, MsgBoxStyle.Information, "DebugMode - Request")
            End If

            Dim encoding As New System.Text.UTF8Encoding()
            Dim bytes As Byte() = encoding.GetBytes(data)
            Using requestStream As Stream = request.GetRequestStream()
                'Send the data.
                requestStream.Write(bytes, 0, bytes.Length)
            End Using
            Dim response As WebResponse = request.GetResponse
            If CType(response, HttpWebResponse).StatusCode = HttpStatusCode.OK Then
                Dim dataStream As New StreamReader(response.GetResponseStream)
                Dim result As String = dataStream.ReadToEnd

                Dim resp As JSON_resul
                resp = JsonConvert.DeserializeObject(Of JSON_resul)(result)

                If resp.CodRespuesta = "00" Then
                    UltimaTransaccionAprobada = resp.nombreVoucher
                Else
                    UltimaTransaccionAprobada = ""
                End If

            End If
        Catch ex As Exception
            'MsgBox(Err.Description, , "Ultima Transaccion Aprobada VPosRest")
            Codigo = "-1"
            Mensaje = "Error al intentar establecer comunicación con el servicio. Información Adicional:" + vbNewLine +
            Err.Description + " (" + Err.Number.ToString() + ")"
            Return String.Empty
        End Try

    End Function

    Public Function Cierre() As Boolean
        Try
            LimpiarVariables()
            Dim request As WebRequest = System.Net.WebRequest.Create(Link)
            request.Method = "POST"
            request.ContentType = "application/json;charset=utf-8"
            Dim data As New String("{ ""accion"":""cierre""}")

            If DebugMode Then
                MsgBox(data, MsgBoxStyle.Information, "DebugMode - Request")
            End If

            Dim encoding As New System.Text.UTF8Encoding()
            Dim bytes As Byte() = encoding.GetBytes(data)
            Using requestStream As Stream = request.GetRequestStream()
                'Send the data.
                requestStream.Write(bytes, 0, bytes.Length)
            End Using
            Dim response As WebResponse = request.GetResponse
            If CType(response, HttpWebResponse).StatusCode = HttpStatusCode.OK Then
                Dim dataStream As New StreamReader(response.GetResponseStream)
                Dim result As String = dataStream.ReadToEnd

                Dim resp As JSON_resul
                resp = JsonConvert.DeserializeObject(Of JSON_resul)(result)

                Codigo = resp.CodRespuesta
                Mensaje = resp.mensajeRespuesta
                Voucher = resp.nombreVoucher
                ArchivoCierre = resp.archivoCierre

                If resp.CodRespuesta = "00" Then
                    Return True
                Else
                    Return False
                End If

            End If

        Catch ex As Exception
            'MsgBox(Err.Description, , "Cierre VPosRest")
            Codigo = "-1"
            Mensaje = "Error al intentar establecer comunicación con el servicio. Información Adicional:" + vbNewLine +
            Err.Description + " (" + Err.Number.ToString() + ")"
            Return False
        End Try
    End Function

    Public Function ConformacionDeCheques(Monto As Double, Optional Cedula As String = "") As Boolean
        Try
            LimpiarVariables()
            Dim request As WebRequest = System.Net.WebRequest.Create(Link)
            request.Method = "POST"
            request.ContentType = "application/json;charset=utf-8"
            Dim data As New String("{ ""accion"":""cheque"", ""montoTransaccion"":""" & Monto & """, ""cedula"":""" & Cedula & """ }")

            If DebugMode Then
                MsgBox(data, MsgBoxStyle.Information, "DebugMode - Request")
            End If

            Dim encoding As New System.Text.UTF8Encoding()
            Dim bytes As Byte() = encoding.GetBytes(data)
            Using requestStream As Stream = request.GetRequestStream()
                'Send the data.
                requestStream.Write(bytes, 0, bytes.Length)
            End Using
            Dim response As WebResponse = request.GetResponse
            If CType(response, HttpWebResponse).StatusCode = HttpStatusCode.OK Then
                Dim dataStream As New StreamReader(response.GetResponseStream)
                Dim result As String = dataStream.ReadToEnd

                Dim resp As JSON_resul
                resp = JsonConvert.DeserializeObject(Of JSON_resul)(result)

                Codigo = resp.CodRespuesta
                Mensaje = resp.mensajeRespuesta
                Voucher = resp.nombreVoucher
                'Referencia = resp.numeroReferencia
                'NumTarjeta = resp.numeroTarjeta
                'NumCedula = resp.cedula
                MontoCompra = resp.montoTransaccion
                'MontoAvanceEfectivo = resp.montoAvance
                'MontoDeServicios = resp.montoServicios
                'MontoDeServiciosAprobado = resp.montoServiciosAprobado
                'TipoCta = resp.tipoCuenta
                'TipoDeTarjeta = resp.tipoTarjeta
                'FechaExp = resp.fechaExpiracion
                HayCopiaVoucher = resp.existeCopiaVoucher
                FechaTrans = resp.fechaTransaccion
                HoraTrans = resp.horaTransaccion
                TerminalV = resp.terminalVirtual
                TipoTrans = resp.tipoTransaccion
                NumAutorizacion = resp.numeroAutorizacion
                CodigoDeAfiliacion = resp.codigoAfiliacion
                TerminalID = resp.tid
                'NumRef = resp.numeroReferencia
                'NombreBancoAutorizador = resp.nombreAutorizador
                'CodigoBancoAdquiriente = resp.codigoAdquiriente
                NumLote = resp.numeroLote
                TipoProd = resp.tipoProducto
                BancoCheque = resp.bancoEmisorCheque
                'NumCuenta = resp.numeroCuenta
                NumCheque = resp.numeroCheque
                'ArchivoCierre = resp.archivoCierre

                If resp.CodRespuesta = "00" Then
                    Return True
                Else
                    Return False
                End If
            End If
        Catch ex As Exception
            'MsgBox(Err.Description, , "Conformacion De Cheque VPosRest")
            Codigo = "-1"
            Mensaje = "Error al intentar establecer comunicación con el servicio. Información Adicional:" + vbNewLine +
            Err.Description + " (" + Err.Number.ToString() + ")"
            Return False
        End Try
    End Function

    Public Function AvanceDeEfectivo(MontoCompra As Double, MontoAvance As Double, Optional Cedula As String = "") As Boolean
        Try
            LimpiarVariables()
            Dim request As WebRequest = System.Net.WebRequest.Create(Link)
            request.Method = "POST"
            request.ContentType = "application/json;charset=utf-8"
            Dim data As New String("{ ""accion"":""avanceEfectivo"", ""montoTransaccion"":""" & MontoCompra & """, ""montoAvanceRetiro"":""" & MontoAvance & """, ""cedula"":""" & Cedula & """ }")

            If DebugMode Then
                MsgBox(data, MsgBoxStyle.Information, "DebugMode - Request")
            End If

            Dim encoding As New System.Text.UTF8Encoding()
            Dim bytes As Byte() = encoding.GetBytes(data)
            Using requestStream As Stream = request.GetRequestStream()
                'Send the data.
                requestStream.Write(bytes, 0, bytes.Length)
            End Using
            Dim response As WebResponse = request.GetResponse
            If CType(response, HttpWebResponse).StatusCode = HttpStatusCode.OK Then
                Dim dataStream As New StreamReader(response.GetResponseStream)
                Dim result As String = dataStream.ReadToEnd

                Dim resp As JSON_resul
                resp = JsonConvert.DeserializeObject(Of JSON_resul)(result)

                Codigo = resp.CodRespuesta
                Mensaje = resp.mensajeRespuesta
                Voucher = resp.nombreVoucher
                Referencia = resp.numeroReferencia
                NumTarjeta = resp.numeroTarjeta
                NumCedula = resp.cedula
                MontoCompra = resp.montoTransaccion
                MontoAvanceEfectivo = resp.montoAvance
                'MontoDeServicios = resp.montoServicios
                'MontoDeServiciosAprobado = resp.montoServiciosAprobado
                TipoCta = resp.tipoCuenta
                TipoDeTarjeta = resp.tipoTarjeta
                FechaExp = resp.fechaExpiracion
                HayCopiaVoucher = resp.existeCopiaVoucher
                FechaTrans = resp.fechaTransaccion
                HoraTrans = resp.horaTransaccion
                TerminalV = resp.terminalVirtual
                TipoTrans = resp.tipoTransaccion
                NumAutorizacion = resp.numeroAutorizacion
                CodigoDeAfiliacion = resp.codigoAfiliacion
                TerminalID = resp.tid
                NumRef = resp.numeroReferencia
                NombreBancoAutorizador = resp.nombreAutorizador
                CodigoBancoAdquiriente = resp.codigoAdquiriente
                NumLote = resp.numeroLote
                TipoProd = resp.tipoProducto
                'BancoCheque = resp.bancoEmisorCheque
                NumCuenta = resp.numeroCuenta
                'NumCheque = resp.numeroCheque
                'ArchivoCierre = resp.archivoCierre

                If resp.CodRespuesta = "00" Then
                    Return True
                Else
                    Return False
                End If
            End If
        Catch ex As Exception
            'MsgBox(Err.Description, , "Avance De Efectivo VPosRest")
            Codigo = "-1"
            Mensaje = "Error al intentar establecer comunicación con el servicio. Información Adicional:" + vbNewLine +
            Err.Description + " (" + Err.Number.ToString() + ")"
            Return False
        End Try
    End Function

    Public Function AvanceDeRetiro(MontoCompra As Double, MontoAvance As Double, Optional Cedula As String = "") As Boolean
        Try
            LimpiarVariables()
            Dim request As WebRequest = System.Net.WebRequest.Create(Link)
            request.Method = "POST"
            request.ContentType = "application/json;charset=utf-8"
            Dim data As New String("{ ""accion"":""avanceRetiro"", ""montoTransaccion"":""" & MontoCompra & """, ""montoAvanceRetiro"":""" & MontoAvance & """, ""cedula"":""" & Cedula & """ }")

            If DebugMode Then
                MsgBox(data, MsgBoxStyle.Information, "DebugMode - Request")
            End If

            Dim encoding As New System.Text.UTF8Encoding()
            Dim bytes As Byte() = encoding.GetBytes(data)
            Using requestStream As Stream = request.GetRequestStream()
                'Send the data.
                requestStream.Write(bytes, 0, bytes.Length)
            End Using
            Dim response As WebResponse = request.GetResponse
            If CType(response, HttpWebResponse).StatusCode = HttpStatusCode.OK Then
                Dim dataStream As New StreamReader(response.GetResponseStream)
                Dim result As String = dataStream.ReadToEnd

                Dim resp As JSON_resul
                resp = JsonConvert.DeserializeObject(Of JSON_resul)(result)

                Codigo = resp.CodRespuesta
                Mensaje = resp.mensajeRespuesta
                Voucher = resp.nombreVoucher
                Referencia = resp.numeroReferencia
                NumTarjeta = resp.numeroTarjeta
                NumCedula = resp.cedula
                MontoCompra = resp.montoTransaccion
                MontoAvanceEfectivo = resp.montoAvance
                'MontoDeServicios = resp.montoServicios
                'MontoDeServiciosAprobado = resp.montoServiciosAprobado
                TipoCta = resp.tipoCuenta
                TipoDeTarjeta = resp.tipoTarjeta
                FechaExp = resp.fechaExpiracion
                HayCopiaVoucher = resp.existeCopiaVoucher
                FechaTrans = resp.fechaTransaccion
                HoraTrans = resp.horaTransaccion
                TerminalV = resp.terminalVirtual
                TipoTrans = resp.tipoTransaccion
                NumAutorizacion = resp.numeroAutorizacion
                CodigoDeAfiliacion = resp.codigoAfiliacion
                TerminalID = resp.tid
                NumRef = resp.numeroReferencia
                NombreBancoAutorizador = resp.nombreAutorizador
                CodigoBancoAdquiriente = resp.codigoAdquiriente
                NumLote = resp.numeroLote
                TipoProd = resp.tipoProducto
                'BancoCheque = resp.bancoEmisorCheque
                NumCuenta = resp.numeroCuenta
                'NumCheque = resp.numeroCheque
                'ArchivoCierre = resp.archivoCierre

                If resp.CodRespuesta = "00" Then
                    Return True
                Else
                    Return False
                End If
            End If
        Catch ex As Exception
            'MsgBox(Err.Description, , "Avance De Efectivo VPosRest")
            Codigo = "-1"
            Mensaje = "Error al intentar establecer comunicación con el servicio. Información Adicional:" + vbNewLine +
            Err.Description + " (" + Err.Number.ToString() + ")"
            Return False
        End Try
    End Function

    Public Function ImpresionUltimoCierre() As Boolean
        Try
            LimpiarVariables()
            Dim request As WebRequest = System.Net.WebRequest.Create(Link)
            request.Method = "POST"
            request.ContentType = "application/json;charset=utf-8"
            Dim data As New String("{ ""accion"":""ultimoCierre""}")

            If DebugMode Then
                MsgBox(data, MsgBoxStyle.Information, "DebugMode - Request")
            End If

            Dim encoding As New System.Text.UTF8Encoding()
            Dim bytes As Byte() = encoding.GetBytes(data)
            Using requestStream As Stream = request.GetRequestStream()
                'Send the data.
                requestStream.Write(bytes, 0, bytes.Length)
            End Using
            Dim response As WebResponse = request.GetResponse
            If CType(response, HttpWebResponse).StatusCode = HttpStatusCode.OK Then
                Dim dataStream As New StreamReader(response.GetResponseStream)
                Dim result As String = dataStream.ReadToEnd

                Dim resp As JSON_resul
                resp = JsonConvert.DeserializeObject(Of JSON_resul)(result)

                Codigo = resp.CodRespuesta
                Mensaje = resp.mensajeRespuesta
                Voucher = resp.nombreVoucher
                ArchivoCierre = resp.archivoCierre

                If resp.CodRespuesta = "00" Then
                    Return True
                Else
                    Return False
                End If

            End If
        Catch ex As Exception
            'MsgBox(Err.Description, , "Impresión del Último Cierre VPosRest")
            Codigo = "-1"
            Mensaje = "Error al intentar establecer comunicación con el servicio. Información Adicional:" + vbNewLine +
            Err.Description + " (" + Err.Number.ToString() + ")"
            Return False
        End Try
    End Function

    ' Metodos SUMA CARDS

    Public Function SUMA_CARDS_LeerTarjetaCards() As Boolean

        Try

            LimpiarVariables()

            Dim TmpRequest As WebRequest = System.Net.WebRequest.Create(URL_Cards)
            TmpRequest.Method = "POST"
            TmpRequest.ContentType = "application/json;charset=utf-8"

            Dim Data As New String("{ ""accion"":""leerTarjetaCards"" }")

            If DebugMode Then
                MsgBox(Data, MsgBoxStyle.Information, "DebugMode - Request")
            End If

            Dim Encoding As New System.Text.UTF8Encoding()
            Dim Bytes As Byte() = Encoding.GetBytes(Data)

            Using RequestStream As Stream = tmpRequest.GetRequestStream()
                'Send the data.
                RequestStream.Write(Bytes, 0, Bytes.Length)
            End Using

            Dim TmpResponse As WebResponse = TmpRequest.GetResponse

            If CType(TmpResponse, HttpWebResponse).StatusCode = HttpStatusCode.OK Then

                Dim DataStream As New StreamReader(TmpResponse.GetResponseStream)
                Dim Result As String = DataStream.ReadToEnd

                Dim Resp As JSON_Resul_Cards
                Resp = JsonConvert.DeserializeObject(Of JSON_Resul_Cards)(Result)

                Codigo = Resp.CodRespuesta
                Mensaje = Resp.mensajeRespuesta
                Voucher = Resp.nombreVoucher
                Referencia = Resp.numeroReferencia
                NumTarjeta = Resp.numeroTarjeta
                NumCedula = Resp.cedula
                MontoCompra = Resp.montoTransaccion
                FechaExp = Resp.fechaExpiracion
                HayCopiaVoucher = Resp.existeCopiaVoucher
                FechaTrans = Resp.fechaTransaccion
                HoraTrans = Resp.horaTransaccion
                TerminalV = Resp.terminalVirtual
                TipoTrans = Resp.tipoTransaccion
                NumAutorizacion = Resp.numeroAutorizacion
                CodigoDeAfiliacion = Resp.codigoAfiliacion
                TerminalID = Resp.tid
                NumRef = Resp.numeroReferencia
                NombreBancoAutorizador = Resp.nombreAutorizador
                CodigoBancoAdquiriente = Resp.codigoAdquiriente
                NumLote = Resp.numeroLote

                If Resp.CodRespuesta = "00" Then
                    Return True
                Else
                    Return False
                End If

            Else
                Err.Raise(CInt(CType(TmpResponse, HttpWebResponse).StatusCode), , "Error HTTP Status Failed")
                Return False
            End If

        Catch AnyEx As Exception

            'MsgBox(Err.Description, , "Compra Con Tarjetas VPosRest")

            Codigo = "-1"
            Mensaje = "Error al intentar establecer comunicación con el servicio (Cards). Información Adicional:" + vbNewLine +
            Err.Description + " (" + Err.Number.ToString() + ")"

            Return False

        End Try

    End Function

    'Public Function SUMA_CARDS_GetNumeroTarjeta() As String

    '    Try

    '        'LimpiarVariables()

    '        Dim TmpRequest As WebRequest = System.Net.WebRequest.Create(URL_Cards)
    '        TmpRequest.Method = "POST"
    '        TmpRequest.ContentType = "application/json;charset=utf-8"

    '        Dim Data As New String("{ ""accion"":""leerTarjetaCards"" }")
    '        Dim Encoding As New System.Text.UTF8Encoding()
    '        Dim Bytes As Byte() = Encoding.GetBytes(Data)

    '        Using RequestStream As Stream = TmpRequest.GetRequestStream()
    '            'Send the data.
    '            RequestStream.Write(Bytes, 0, Bytes.Length)
    '        End Using

    '        Dim TmpResponse As WebResponse = TmpRequest.GetResponse

    '        If CType(TmpResponse, HttpWebResponse).StatusCode = HttpStatusCode.OK Then

    '            Dim DataStream As New StreamReader(TmpResponse.GetResponseStream)
    '            Dim Result As String = DataStream.ReadToEnd

    '            Dim Resp As JSON_Resul_Cards
    '            Resp = JsonConvert.DeserializeObject(Of JSON_Resul_Cards)(Result)

    '            Codigo = Resp.CodRespuesta
    '            Mensaje = Resp.mensajeRespuesta
    '            NumTarjeta = Resp.numeroTarjeta
    '            NumeroDeTarjetaEnmascarado = Resp.numeroTarjetaEnmascarado

    '            Return NumTarjeta

    '        Else
    '            Err.Raise(CInt(CType(TmpResponse, HttpWebResponse).StatusCode), , "Error HTTP Status Failed")
    '            Return String.Empty
    '        End If

    '    Catch AnyEx As Exception

    '        'MsgBox(Err.Description, , "Compra Con Tarjetas VPosRest")

    '        Codigo = "-1"
    '        Mensaje = "Error al intentar establecer comunicación con el servicio (Cards). Información Adicional:" + vbNewLine+
    '        Err.Description + " (" + Err.Number.ToString() + ")"

    '        Return String.Empty

    '    End Try

    'End Function

    Public Function SUMA_CARDS_ConsultaPuntosTFisica(ByVal pCedula As String, _
    ByVal pNumeroTarjeta As String) As Boolean

        Try

            LimpiarVariables()

            Dim TmpRequest As WebRequest = System.Net.WebRequest.Create(URL_Cards)
            TmpRequest.Method = "POST"
            TmpRequest.ContentType = "application/json;charset=utf-8"

            Dim Data As New String("{ ""accion"":""consultarPuntosTFisica"", " +
            """cedula"":""" + pCedula + """, " +
            """numeroTarjeta"":""" + pNumeroTarjeta + """ }")

            If DebugMode Then
                MsgBox(Data, MsgBoxStyle.Information, "DebugMode - Request")
            End If

            Dim Encoding As New System.Text.UTF8Encoding()
            Dim Bytes As Byte() = Encoding.GetBytes(Data)

            Using RequestStream As Stream = TmpRequest.GetRequestStream()
                'Send the data.
                RequestStream.Write(Bytes, 0, Bytes.Length)
            End Using

            Dim TmpResponse As WebResponse = TmpRequest.GetResponse

            If CType(TmpResponse, HttpWebResponse).StatusCode = HttpStatusCode.OK Then

                Dim DataStream As New StreamReader(TmpResponse.GetResponseStream)
                Dim Result As String = DataStream.ReadToEnd

                Dim Resp As JSON_Resul_Cards
                Resp = JsonConvert.DeserializeObject(Of JSON_Resul_Cards)(Result)

                Codigo = Resp.CodRespuesta
                Mensaje = Resp.mensajeRespuesta
                Voucher = Resp.nombreVoucher
                Referencia = Resp.numeroReferencia
                NumTarjeta = Resp.numeroTarjeta
                NumCedula = Resp.cedula
                MontoCompra = Resp.montoTransaccion
                FechaExp = Resp.fechaExpiracion
                HayCopiaVoucher = Resp.existeCopiaVoucher
                FechaTrans = Resp.fechaTransaccion
                HoraTrans = Resp.horaTransaccion
                TerminalV = Resp.terminalVirtual
                TipoTrans = Resp.tipoTransaccion
                NumAutorizacion = Resp.numeroAutorizacion
                CodigoDeAfiliacion = Resp.codigoAfiliacion
                TerminalID = Resp.tid
                NumRef = Resp.numeroReferencia
                NombreBancoAutorizador = Resp.nombreAutorizador
                CodigoBancoAdquiriente = Resp.codigoAdquiriente
                NumLote = Resp.numeroLote
                SaldoDisponible = Resp.saldoDisponible

                If Resp.CodRespuesta = "00" Then
                    Return True
                Else
                    Return False
                End If

            Else
                Err.Raise(CInt(CType(TmpResponse, HttpWebResponse).StatusCode), , "Error HTTP Status Failed")
                Return False
            End If

        Catch AnyEx As Exception

            'MsgBox(Err.Description, , "Compra Con Tarjetas VPosRest")

            Codigo = "-1"
            Mensaje = "Error al intentar establecer comunicación con el servicio (Cards). Información Adicional:" + vbNewLine +
            Err.Description + " (" + Err.Number.ToString() + ")"

            Return False

        End Try

    End Function

    Public Function SUMA_CARDS_CanjePuntosTFisica(ByVal pCedula As String, _
    ByVal pNumeroTarjeta As String, _
    ByVal pPuntosCanje As String) As Boolean

        Try

            LimpiarVariables()

            Dim TmpRequest As WebRequest = System.Net.WebRequest.Create(URL_Cards)
            TmpRequest.Method = "POST"
            TmpRequest.ContentType = "application/json;charset=utf-8"

            Dim Data As New String("{ ""accion"":""canjePuntosTFisica"", " +
            """cedula"":""" + pCedula + """, " +
            """numeroTarjeta"":""" + pNumeroTarjeta + """, " +
            """puntosCanjear"":""" + pPuntosCanje + """ }")

            If DebugMode Then
                MsgBox(Data, MsgBoxStyle.Information, "DebugMode - Request")
            End If

            Dim Encoding As New System.Text.UTF8Encoding()
            Dim Bytes As Byte() = Encoding.GetBytes(Data)

            Using RequestStream As Stream = TmpRequest.GetRequestStream()
                'Send the data.
                RequestStream.Write(Bytes, 0, Bytes.Length)
            End Using

            Dim TmpResponse As WebResponse = TmpRequest.GetResponse

            If CType(TmpResponse, HttpWebResponse).StatusCode = HttpStatusCode.OK Then

                Dim DataStream As New StreamReader(TmpResponse.GetResponseStream)
                Dim Result As String = DataStream.ReadToEnd

                Dim Resp As JSON_Resul_Cards
                Resp = JsonConvert.DeserializeObject(Of JSON_Resul_Cards)(Result)

                Codigo = Resp.CodRespuesta
                Mensaje = Resp.mensajeRespuesta
                Voucher = Resp.nombreVoucher
                Referencia = Resp.numeroReferencia
                NumTarjeta = Resp.numeroTarjeta
                NumCedula = Resp.cedula
                MontoCompra = Resp.montoTransaccion
                FechaExp = Resp.fechaExpiracion
                HayCopiaVoucher = Resp.existeCopiaVoucher
                FechaTrans = Resp.fechaTransaccion
                HoraTrans = Resp.horaTransaccion
                TerminalV = Resp.terminalVirtual
                TipoTrans = Resp.tipoTransaccion
                TipoDeTarjeta = Resp.tipoProducto
                NumAutorizacion = Resp.numeroAutorizacion
                CodigoDeAfiliacion = Resp.codigoAfiliacion
                TerminalID = Resp.tid
                NumRef = Resp.numeroReferencia
                NombreBancoAutorizador = Resp.nombreAutorizador
                CodigoBancoAdquiriente = Resp.codigoAdquiriente
                NumLote = Resp.numeroLote

                If Resp.CodRespuesta = "00" Then
                    Return True
                Else
                    Return False
                End If

            Else
                Err.Raise(CInt(CType(TmpResponse, HttpWebResponse).StatusCode), , "Error HTTP Status Failed")
                Return False
            End If

        Catch AnyEx As Exception

            'MsgBox(Err.Description, , "Compra Con Tarjetas VPosRest")

            Codigo = "-1"
            Mensaje = "Error al intentar establecer comunicación con el servicio (Cards). Información Adicional:" + vbNewLine +
            Err.Description + " (" + Err.Number.ToString() + ")"

            Return False

        End Try

    End Function

    Public Function SUMA_CARDS_ConsultaPuntosTVirtual(ByVal pCedula As String, _
    ByVal pNumeroTarjeta As String, _
    ByVal pFechaVencimiento As String) As Boolean

        Try

            LimpiarVariables()

            Dim TmpRequest As WebRequest = System.Net.WebRequest.Create(URL_Cards)
            TmpRequest.Method = "POST"
            TmpRequest.ContentType = "application/json;charset=utf-8"

            Dim Data As New String("{ ""accion"":""consultarPuntosTVirtual"", " +
            """cedula"":""" + pCedula + """, " +
            """numeroTarjeta"":""" + pNumeroTarjeta + """, " +
            """fechaVcmto"":""" + pFechaVencimiento + """ }")

            If DebugMode Then
                MsgBox(Data, MsgBoxStyle.Information, "DebugMode - Request")
            End If

            Dim Encoding As New System.Text.UTF8Encoding()
            Dim Bytes As Byte() = Encoding.GetBytes(Data)

            Using RequestStream As Stream = TmpRequest.GetRequestStream()
                'Send the data.
                RequestStream.Write(Bytes, 0, Bytes.Length)
            End Using

            Dim TmpResponse As WebResponse = TmpRequest.GetResponse

            If CType(TmpResponse, HttpWebResponse).StatusCode = HttpStatusCode.OK Then

                Dim DataStream As New StreamReader(TmpResponse.GetResponseStream)
                Dim Result As String = DataStream.ReadToEnd

                Dim Resp As JSON_Resul_Cards
                Resp = JsonConvert.DeserializeObject(Of JSON_Resul_Cards)(Result)

                Codigo = Resp.CodRespuesta
                Mensaje = Resp.mensajeRespuesta
                Voucher = Resp.nombreVoucher
                Referencia = Resp.numeroReferencia
                NumTarjeta = Resp.numeroTarjeta
                NumCedula = Resp.cedula
                MontoCompra = Resp.montoTransaccion
                FechaExp = Resp.fechaExpiracion
                HayCopiaVoucher = Resp.existeCopiaVoucher
                FechaTrans = Resp.fechaTransaccion
                HoraTrans = Resp.horaTransaccion
                TerminalV = Resp.terminalVirtual
                TipoTrans = Resp.tipoTransaccion
                NumAutorizacion = Resp.numeroAutorizacion
                CodigoDeAfiliacion = Resp.codigoAfiliacion
                TerminalID = Resp.tid
                NumRef = Resp.numeroReferencia
                NombreBancoAutorizador = Resp.nombreAutorizador
                CodigoBancoAdquiriente = Resp.codigoAdquiriente
                NumLote = Resp.numeroLote
                SaldoDisponible = Resp.saldoDisponible

                If Resp.CodRespuesta = "00" Then
                    Return True
                Else
                    Return False
                End If

            Else
                Err.Raise(CInt(CType(TmpResponse, HttpWebResponse).StatusCode), , "Error HTTP Status Failed")
                Return False
            End If

        Catch AnyEx As Exception

            'MsgBox(Err.Description, , "Compra Con Tarjetas VPosRest")

            Codigo = "-1"
            Mensaje = "Error al intentar establecer comunicación con el servicio (Cards). Información Adicional:" + vbNewLine +
            Err.Description + " (" + Err.Number.ToString() + ")"

            Return False

        End Try

    End Function

    Public Function SUMA_CARDS_CanjePuntosTVirtual(ByVal pCedula As String, _
    ByVal pNumeroTarjeta As String, _
    ByVal pPuntosCanje As String) As Boolean

        Try

            LimpiarVariables()

            Dim TmpRequest As WebRequest = System.Net.WebRequest.Create(URL_Cards)
            TmpRequest.Method = "POST"
            TmpRequest.ContentType = "application/json;charset=utf-8"

            Dim Data As New String("{ ""accion"":""canjePuntosTVirtual"", " +
            """cedula"":""" + pCedula + """, " +
            """numeroTarjeta"":""" + pNumeroTarjeta + """, " +
            """puntosCanjear"":""" + pPuntosCanje + """ }")

            If DebugMode Then
                MsgBox(Data, MsgBoxStyle.Information, "DebugMode - Request")
            End If

            Dim Encoding As New System.Text.UTF8Encoding()
            Dim Bytes As Byte() = Encoding.GetBytes(Data)

            Using RequestStream As Stream = TmpRequest.GetRequestStream()
                'Send the data.
                RequestStream.Write(Bytes, 0, Bytes.Length)
            End Using

            Dim TmpResponse As WebResponse = TmpRequest.GetResponse

            If CType(TmpResponse, HttpWebResponse).StatusCode = HttpStatusCode.OK Then

                Dim DataStream As New StreamReader(TmpResponse.GetResponseStream)
                Dim Result As String = DataStream.ReadToEnd

                Dim Resp As JSON_Resul_Cards
                Resp = JsonConvert.DeserializeObject(Of JSON_Resul_Cards)(Result)

                Codigo = Resp.CodRespuesta
                Mensaje = Resp.mensajeRespuesta
                Voucher = Resp.nombreVoucher
                Referencia = Resp.numeroReferencia
                NumTarjeta = Resp.numeroTarjeta
                NumCedula = Resp.cedula
                MontoCompra = Resp.montoTransaccion
                FechaExp = Resp.fechaExpiracion
                HayCopiaVoucher = Resp.existeCopiaVoucher
                FechaTrans = Resp.fechaTransaccion
                HoraTrans = Resp.horaTransaccion
                TerminalV = Resp.terminalVirtual
                TipoTrans = Resp.tipoTransaccion
                TipoDeTarjeta = Resp.tipoProducto
                NumAutorizacion = Resp.numeroAutorizacion
                CodigoDeAfiliacion = Resp.codigoAfiliacion
                TerminalID = Resp.tid
                NumRef = Resp.numeroReferencia
                NombreBancoAutorizador = Resp.nombreAutorizador
                CodigoBancoAdquiriente = Resp.codigoAdquiriente
                NumLote = Resp.numeroLote

                If Resp.CodRespuesta = "00" Then
                    Return True
                Else
                    Return False
                End If

            Else
                Err.Raise(CInt(CType(TmpResponse, HttpWebResponse).StatusCode), , "Error HTTP Status Failed")
                Return False
            End If

        Catch AnyEx As Exception

            'MsgBox(Err.Description, , "Compra Con Tarjetas VPosRest")

            Codigo = "-1"
            Mensaje = "Error al intentar establecer comunicación con el servicio (Cards). Información Adicional:" + vbNewLine +
            Err.Description + " (" + Err.Number.ToString() + ")"

            Return False

        End Try

    End Function

    Public Function SUMA_CARDS_ConsultaSaldoTFisica(ByVal pCedula As String,
    ByVal pNumeroTarjeta As String) As Boolean

        Try

            LimpiarVariables()

            Dim TmpRequest As WebRequest = System.Net.WebRequest.Create(URL_Cards)
            TmpRequest.Method = "POST"
            TmpRequest.ContentType = "application/json;charset=utf-8"

            Dim Data As New String("{ ""accion"":""consultarSaldoCards"", " +
            """cedula"":""" + pCedula + """, " +
            """numeroTarjeta"":""" + pNumeroTarjeta + """ }")

            If DebugMode Then
                MsgBox(Data, MsgBoxStyle.Information, "DebugMode - Request")
            End If

            Dim Encoding As New System.Text.UTF8Encoding()
            Dim Bytes As Byte() = Encoding.GetBytes(Data)

            Using RequestStream As Stream = TmpRequest.GetRequestStream()
                'Send the data.
                RequestStream.Write(Bytes, 0, Bytes.Length)
            End Using

            Dim TmpResponse As WebResponse = TmpRequest.GetResponse

            If CType(TmpResponse, HttpWebResponse).StatusCode = HttpStatusCode.OK Then

                Dim DataStream As New StreamReader(TmpResponse.GetResponseStream)
                Dim Result As String = DataStream.ReadToEnd

                Dim Resp As JSON_Resul_Cards
                Resp = JsonConvert.DeserializeObject(Of JSON_Resul_Cards)(Result)

                Codigo = Resp.CodRespuesta
                Mensaje = Resp.mensajeRespuesta
                Voucher = Resp.nombreVoucher
                Referencia = Resp.numeroReferencia
                NumTarjeta = Resp.numeroTarjeta
                NumCedula = Resp.cedula
                MontoCompra = Resp.montoTransaccion
                FechaExp = Resp.fechaExpiracion
                HayCopiaVoucher = Resp.existeCopiaVoucher
                FechaTrans = Resp.fechaTransaccion
                HoraTrans = Resp.horaTransaccion
                TerminalV = Resp.terminalVirtual
                TipoTrans = Resp.tipoTransaccion
                NumAutorizacion = Resp.numeroAutorizacion
                CodigoDeAfiliacion = Resp.codigoAfiliacion
                TerminalID = Resp.tid
                NumRef = Resp.numeroReferencia
                NombreBancoAutorizador = Resp.nombreAutorizador
                CodigoBancoAdquiriente = Resp.codigoAdquiriente
                NumLote = Resp.numeroLote
                SaldoDisponible = Resp.saldoDisponible

                If Resp.CodRespuesta = "00" Then
                    Return True
                Else
                    Return False
                End If

            Else
                Err.Raise(CInt(CType(TmpResponse, HttpWebResponse).StatusCode), , "Error HTTP Status Failed")
                Return False
            End If

        Catch AnyEx As Exception

            'MsgBox(Err.Description, , "Compra Con Tarjetas VPosRest")

            Codigo = "-1"
            Mensaje = "Error al intentar establecer comunicación con el servicio (Cards). Información Adicional:" + vbNewLine +
            Err.Description + " (" + Err.Number.ToString() + ")"

            Return False

        End Try

    End Function

    Public Function SUMA_CARDS_ConsultaSaldoTVirtual(ByVal pCedula As String,
    ByVal pNumeroTarjeta As String) As Boolean

        Try

            LimpiarVariables()

            Dim TmpRequest As WebRequest = System.Net.WebRequest.Create(URL_Cards)
            TmpRequest.Method = "POST"
            TmpRequest.ContentType = "application/json;charset=utf-8"

            Dim Data As New String("{ ""accion"":""consultaSaldoTVirtual"", " +
            """cedula"":""" + pCedula + """, " +
            """numeroTarjeta"":""" + pNumeroTarjeta + """ }")

            If DebugMode Then
                MsgBox(Data, MsgBoxStyle.Information, "DebugMode - Request")
            End If

            Dim Encoding As New System.Text.UTF8Encoding()
            Dim Bytes As Byte() = Encoding.GetBytes(Data)

            Using RequestStream As Stream = TmpRequest.GetRequestStream()
                'Send the data.
                RequestStream.Write(Bytes, 0, Bytes.Length)
            End Using

            Dim TmpResponse As WebResponse = TmpRequest.GetResponse

            If CType(TmpResponse, HttpWebResponse).StatusCode = HttpStatusCode.OK Then

                Dim DataStream As New StreamReader(TmpResponse.GetResponseStream)
                Dim Result As String = DataStream.ReadToEnd

                Dim Resp As JSON_Resul_Cards
                Resp = JsonConvert.DeserializeObject(Of JSON_Resul_Cards)(Result)

                Codigo = Resp.CodRespuesta
                Mensaje = Resp.mensajeRespuesta
                Voucher = Resp.nombreVoucher
                Referencia = Resp.numeroReferencia
                NumTarjeta = Resp.numeroTarjeta
                NumCedula = Resp.cedula
                MontoCompra = Resp.montoTransaccion
                FechaExp = Resp.fechaExpiracion
                HayCopiaVoucher = Resp.existeCopiaVoucher
                FechaTrans = Resp.fechaTransaccion
                HoraTrans = Resp.horaTransaccion
                TerminalV = Resp.terminalVirtual
                TipoTrans = Resp.tipoTransaccion
                NumAutorizacion = Resp.numeroAutorizacion
                CodigoDeAfiliacion = Resp.codigoAfiliacion
                TerminalID = Resp.tid
                NumRef = Resp.numeroReferencia
                NombreBancoAutorizador = Resp.nombreAutorizador
                CodigoBancoAdquiriente = Resp.codigoAdquiriente
                NumLote = Resp.numeroLote
                SaldoDisponible = Resp.saldoDisponible

                If Resp.CodRespuesta = "00" Then
                    Return True
                Else
                    Return False
                End If

            Else
                Err.Raise(CInt(CType(TmpResponse, HttpWebResponse).StatusCode), , "Error HTTP Status Failed")
                Return False
            End If

        Catch AnyEx As Exception

            'MsgBox(Err.Description, , "Compra Con Tarjetas VPosRest")

            Codigo = "-1"
            Mensaje = "Error al intentar establecer comunicación con el servicio (Cards). Información Adicional:" + vbNewLine +
            Err.Description + " (" + Err.Number.ToString() + ")"

            Return False

        End Try

    End Function

    Public Function SUMA_CARDS_CompraSaldoTFisica(ByVal pCedula As String, _
    ByVal pNumeroTarjeta As String, _
    ByVal pSaldoAplicar As String) As Boolean

        Try

            LimpiarVariables()

            Dim TmpRequest As WebRequest = System.Net.WebRequest.Create(URL_Cards)
            TmpRequest.Method = "POST"
            TmpRequest.ContentType = "application/json;charset=utf-8"

            Dim Data As New String("{ ""accion"":""compraCards"", " +
            """cedula"":""" + pCedula + """, " +
            """numeroTarjeta"":""" + pNumeroTarjeta + """, " +
            """saldoPagar"":""" + pSaldoAplicar + """ }")

            If DebugMode Then
                MsgBox(Data, MsgBoxStyle.Information, "DebugMode - Request")
            End If

            Dim Encoding As New System.Text.UTF8Encoding()
            Dim Bytes As Byte() = Encoding.GetBytes(Data)

            Using RequestStream As Stream = TmpRequest.GetRequestStream()
                'Send the data.
                RequestStream.Write(Bytes, 0, Bytes.Length)
            End Using

            Dim TmpResponse As WebResponse = TmpRequest.GetResponse

            If CType(TmpResponse, HttpWebResponse).StatusCode = HttpStatusCode.OK Then

                Dim DataStream As New StreamReader(TmpResponse.GetResponseStream)
                Dim Result As String = DataStream.ReadToEnd

                Dim Resp As JSON_Resul_Cards
                Resp = JsonConvert.DeserializeObject(Of JSON_Resul_Cards)(Result)

                Codigo = Resp.CodRespuesta
                Mensaje = Resp.mensajeRespuesta
                Voucher = Resp.nombreVoucher
                Referencia = Resp.numeroReferencia
                NumTarjeta = Resp.numeroTarjeta
                NumCedula = Resp.cedula
                MontoCompra = Resp.montoTransaccion
                FechaExp = Resp.fechaExpiracion
                HayCopiaVoucher = Resp.existeCopiaVoucher
                FechaTrans = Resp.fechaTransaccion
                HoraTrans = Resp.horaTransaccion
                TerminalV = Resp.terminalVirtual
                TipoTrans = Resp.tipoTransaccion
                TipoDeTarjeta = Resp.tipoProducto
                NumAutorizacion = Resp.numeroAutorizacion
                CodigoDeAfiliacion = Resp.codigoAfiliacion
                TerminalID = Resp.tid
                NumRef = Resp.numeroReferencia
                NombreBancoAutorizador = Resp.nombreAutorizador
                CodigoBancoAdquiriente = Resp.codigoAdquiriente
                NumLote = Resp.numeroLote

                If Resp.CodRespuesta = "00" Then
                    Return True
                Else
                    Return False
                End If

            Else
                Err.Raise(CInt(CType(TmpResponse, HttpWebResponse).StatusCode), , "Error HTTP Status Failed")
                Return False
            End If

        Catch AnyEx As Exception

            'MsgBox(Err.Description, , "Compra Con Tarjetas VPosRest")

            Codigo = "-1"
            Mensaje = "Error al intentar establecer comunicación con el servicio (Cards). Información Adicional:" + vbNewLine +
            Err.Description + " (" + Err.Number.ToString() + ")"

            Return False

        End Try

    End Function

    Public Function SUMA_CARDS_AcreditarPuntosTFisica(ByVal pCedula As String, _
    ByVal pPuntosAcreditar As String) As Boolean

        Try

            LimpiarVariables()

            Dim TmpRequest As WebRequest = System.Net.WebRequest.Create(URL_Cards)
            TmpRequest.Method = "POST"
            TmpRequest.ContentType = "application/json;charset=utf-8"

            Dim Data As New String("{ ""accion"":""acreditarPuntosTFisica"", " +
            """cedula"":""" + pCedula + """, " +
            """puntosAcreditar"":""" + pPuntosAcreditar + """ }")

            If DebugMode Then
                MsgBox(Data, MsgBoxStyle.Information, "DebugMode - Request")
            End If

            Dim Encoding As New System.Text.UTF8Encoding()
            Dim Bytes As Byte() = Encoding.GetBytes(Data)

            Using RequestStream As Stream = TmpRequest.GetRequestStream()
                'Send the data.
                RequestStream.Write(Bytes, 0, Bytes.Length)
            End Using

            Dim TmpResponse As WebResponse = TmpRequest.GetResponse

            If CType(TmpResponse, HttpWebResponse).StatusCode = HttpStatusCode.OK Then

                Dim DataStream As New StreamReader(TmpResponse.GetResponseStream)
                Dim Result As String = DataStream.ReadToEnd

                Dim Resp As JSON_Resul_Cards
                Resp = JsonConvert.DeserializeObject(Of JSON_Resul_Cards)(Result)

                Codigo = Resp.CodRespuesta
                Mensaje = Resp.mensajeRespuesta
                Voucher = Resp.nombreVoucher
                Referencia = Resp.numeroReferencia
                NumTarjeta = Resp.numeroTarjeta
                NumCedula = Resp.cedula
                MontoCompra = Resp.montoTransaccion
                FechaExp = Resp.fechaExpiracion
                HayCopiaVoucher = Resp.existeCopiaVoucher
                FechaTrans = Resp.fechaTransaccion
                HoraTrans = Resp.horaTransaccion
                TerminalV = Resp.terminalVirtual
                TipoTrans = Resp.tipoTransaccion
                NumAutorizacion = Resp.numeroAutorizacion
                CodigoDeAfiliacion = Resp.codigoAfiliacion
                TerminalID = Resp.tid
                NumRef = Resp.numeroReferencia
                NombreBancoAutorizador = Resp.nombreAutorizador
                CodigoBancoAdquiriente = Resp.codigoAdquiriente
                NumLote = Resp.numeroLote

                If Resp.CodRespuesta = "00" Then
                    Return True
                Else
                    Return False
                End If

            Else
                Err.Raise(CInt(CType(TmpResponse, HttpWebResponse).StatusCode), , "Error HTTP Status Failed")
                Return False
            End If

        Catch AnyEx As Exception

            'MsgBox(Err.Description, , "Compra Con Tarjetas VPosRest")

            Codigo = "-1"
            Mensaje = "Error al intentar establecer comunicación con el servicio (Cards). Información Adicional:" + vbNewLine +
            Err.Description + " (" + Err.Number.ToString() + ")"

            Return False

        End Try

    End Function

    Public Function SUMA_CARDS_AcreditarPuntosTVirtual(ByVal pCedula As String, _
    ByVal pNumeroTarjeta As String, _
    ByVal pFechaVencimiento As String, _
    ByVal pPuntosAcreditar As String) As Boolean

        Try

            LimpiarVariables()

            Dim TmpRequest As WebRequest = System.Net.WebRequest.Create(URL_Cards)
            TmpRequest.Method = "POST"
            TmpRequest.ContentType = "application/json;charset=utf-8"

            Dim Data As New String("{ ""accion"":""acreditarPuntosTVirtual"", " +
            """cedula"":""" + pCedula + """, " +
            """numeroTarjeta"":""" + pNumeroTarjeta + """, " +
            """fechaVcmto"":""" + pFechaVencimiento + """, " +
            """puntosAcreditar"":""" + pPuntosAcreditar + """ }")

            If DebugMode Then
                MsgBox(Data, MsgBoxStyle.Information, "DebugMode - Request")
            End If

            Dim Encoding As New System.Text.UTF8Encoding()
            Dim Bytes As Byte() = Encoding.GetBytes(Data)

            Using RequestStream As Stream = TmpRequest.GetRequestStream()
                'Send the data.
                RequestStream.Write(Bytes, 0, Bytes.Length)
            End Using

            Dim TmpResponse As WebResponse = TmpRequest.GetResponse

            If CType(TmpResponse, HttpWebResponse).StatusCode = HttpStatusCode.OK Then

                Dim DataStream As New StreamReader(TmpResponse.GetResponseStream)
                Dim Result As String = DataStream.ReadToEnd

                Dim Resp As JSON_Resul_Cards
                Resp = JsonConvert.DeserializeObject(Of JSON_Resul_Cards)(Result)

                Codigo = Resp.CodRespuesta
                Mensaje = Resp.mensajeRespuesta
                Voucher = Resp.nombreVoucher
                Referencia = Resp.numeroReferencia
                NumTarjeta = Resp.numeroTarjeta
                NumCedula = Resp.cedula
                MontoCompra = Resp.montoTransaccion
                FechaExp = Resp.fechaExpiracion
                HayCopiaVoucher = Resp.existeCopiaVoucher
                FechaTrans = Resp.fechaTransaccion
                HoraTrans = Resp.horaTransaccion
                TerminalV = Resp.terminalVirtual
                TipoTrans = Resp.tipoTransaccion
                NumAutorizacion = Resp.numeroAutorizacion
                CodigoDeAfiliacion = Resp.codigoAfiliacion
                TerminalID = Resp.tid
                NumRef = Resp.numeroReferencia
                NombreBancoAutorizador = Resp.nombreAutorizador
                CodigoBancoAdquiriente = Resp.codigoAdquiriente
                NumLote = Resp.numeroLote
                SaldoDisponible = Resp.saldoDisponible

                If Resp.CodRespuesta = "00" Then
                    Return True
                Else
                    Return False
                End If

            Else
                Err.Raise(CInt(CType(TmpResponse, HttpWebResponse).StatusCode), , "Error HTTP Status Failed")
                Return False
            End If

        Catch AnyEx As Exception

            'MsgBox(Err.Description, , "Compra Con Tarjetas VPosRest")

            Codigo = "-1"
            Mensaje = "Error al intentar establecer comunicación con el servicio (Cards). Información Adicional:" + vbNewLine +
            Err.Description + " (" + Err.Number.ToString() + ")"

            Return False

        End Try

    End Function

    Public Function SUMA_CARDS_DevolucionPuntosTFisica(ByVal pCedula As String, _
    ByVal pPuntosDevolver As String) As Boolean

        Try

            LimpiarVariables()

            Dim TmpRequest As WebRequest = System.Net.WebRequest.Create(URL_Cards)
            TmpRequest.Method = "POST"
            TmpRequest.ContentType = "application/json;charset=utf-8"

            Dim Data As New String("{ ""accion"":""devolverPuntosTFisica"", " +
            """cedula"":""" + pCedula + """, " +
            """puntosDevolver"":""" + pPuntosDevolver + """ }")

            If DebugMode Then
                MsgBox(Data, MsgBoxStyle.Information, "DebugMode - Request")
            End If

            Dim Encoding As New System.Text.UTF8Encoding()
            Dim Bytes As Byte() = Encoding.GetBytes(Data)

            If DebugMode Then
                MsgBox(Data, MsgBoxStyle.Information, "DebugMode - Request")
            End If

            Using RequestStream As Stream = TmpRequest.GetRequestStream()
                'Send the data.
                RequestStream.Write(Bytes, 0, Bytes.Length)
            End Using

            Dim TmpResponse As WebResponse = TmpRequest.GetResponse

            If CType(TmpResponse, HttpWebResponse).StatusCode = HttpStatusCode.OK Then

                Dim DataStream As New StreamReader(TmpResponse.GetResponseStream)
                Dim Result As String = DataStream.ReadToEnd

                Dim Resp As JSON_Resul_Cards
                Resp = JsonConvert.DeserializeObject(Of JSON_Resul_Cards)(Result)

                Codigo = Resp.CodRespuesta
                Mensaje = Resp.mensajeRespuesta
                Voucher = Resp.nombreVoucher
                Referencia = Resp.numeroReferencia
                NumTarjeta = Resp.numeroTarjeta
                NumCedula = Resp.cedula
                MontoCompra = Resp.montoTransaccion
                FechaExp = Resp.fechaExpiracion
                HayCopiaVoucher = Resp.existeCopiaVoucher
                FechaTrans = Resp.fechaTransaccion
                HoraTrans = Resp.horaTransaccion
                TerminalV = Resp.terminalVirtual
                TipoTrans = Resp.tipoTransaccion
                NumAutorizacion = Resp.numeroAutorizacion
                CodigoDeAfiliacion = Resp.codigoAfiliacion
                TerminalID = Resp.tid
                NumRef = Resp.numeroReferencia
                NombreBancoAutorizador = Resp.nombreAutorizador
                CodigoBancoAdquiriente = Resp.codigoAdquiriente
                NumLote = Resp.numeroLote

                If Resp.CodRespuesta = "00" Then
                    Return True
                Else
                    Return False
                End If

            Else
                Err.Raise(CInt(CType(TmpResponse, HttpWebResponse).StatusCode), , "Error HTTP Status Failed")
                Return False
            End If

        Catch AnyEx As Exception

            'MsgBox(Err.Description, , "Compra Con Tarjetas VPosRest")

            Codigo = "-1"
            Mensaje = "Error al intentar establecer comunicación con el servicio (Cards). Información Adicional:" + vbNewLine +
            Err.Description + " (" + Err.Number.ToString() + ")"

            Return False

        End Try

    End Function

    Public Function SUMA_CARDS_DevolucionPuntosTVirtual(ByVal pCedula As String, _
    ByVal pNumeroTarjeta As String, _
    ByVal pFechaVencimiento As String, _
    ByVal pPuntosDevolver As String) As Boolean

        Try

            LimpiarVariables()

            Dim TmpRequest As WebRequest = System.Net.WebRequest.Create(URL_Cards)
            TmpRequest.Method = "POST"
            TmpRequest.ContentType = "application/json;charset=utf-8"

            Dim Data As New String("{ ""accion"":""devolverPuntosTVirtual"", " +
            """cedula"":""" + pCedula + """, " +
            """numeroTarjeta"":""" + pNumeroTarjeta + """, " +
            """fechaVcmto"":""" + pFechaVencimiento + """, " +
            """puntosDevolver"":""" + pPuntosDevolver + """ }")

            If DebugMode Then
                MsgBox(Data, MsgBoxStyle.Information, "DebugMode - Request")
            End If

            Dim Encoding As New System.Text.UTF8Encoding()
            Dim Bytes As Byte() = Encoding.GetBytes(Data)

            Using RequestStream As Stream = TmpRequest.GetRequestStream()
                'Send the data.
                RequestStream.Write(Bytes, 0, Bytes.Length)
            End Using

            Dim TmpResponse As WebResponse = TmpRequest.GetResponse

            If CType(TmpResponse, HttpWebResponse).StatusCode = HttpStatusCode.OK Then

                Dim DataStream As New StreamReader(TmpResponse.GetResponseStream)
                Dim Result As String = DataStream.ReadToEnd

                Dim Resp As JSON_Resul_Cards
                Resp = JsonConvert.DeserializeObject(Of JSON_Resul_Cards)(Result)

                Codigo = Resp.CodRespuesta
                Mensaje = Resp.mensajeRespuesta
                Voucher = Resp.nombreVoucher
                Referencia = Resp.numeroReferencia
                NumTarjeta = Resp.numeroTarjeta
                NumCedula = Resp.cedula
                MontoCompra = Resp.montoTransaccion
                FechaExp = Resp.fechaExpiracion
                HayCopiaVoucher = Resp.existeCopiaVoucher
                FechaTrans = Resp.fechaTransaccion
                HoraTrans = Resp.horaTransaccion
                TerminalV = Resp.terminalVirtual
                TipoTrans = Resp.tipoTransaccion
                NumAutorizacion = Resp.numeroAutorizacion
                CodigoDeAfiliacion = Resp.codigoAfiliacion
                TerminalID = Resp.tid
                NumRef = Resp.numeroReferencia
                NombreBancoAutorizador = Resp.nombreAutorizador
                CodigoBancoAdquiriente = Resp.codigoAdquiriente
                NumLote = Resp.numeroLote
                SaldoDisponible = Resp.saldoDisponible

                If Resp.CodRespuesta = "00" Then
                    Return True
                Else
                    Return False
                End If

            Else
                Err.Raise(CInt(CType(TmpResponse, HttpWebResponse).StatusCode), , "Error HTTP Status Failed")
                Return False
            End If

        Catch AnyEx As Exception

            'MsgBox(Err.Description, , "Compra Con Tarjetas VPosRest")

            Codigo = "-1"
            Mensaje = "Error al intentar establecer comunicación con el servicio (Cards). Información Adicional:" + vbNewLine +
            Err.Description + " (" + Err.Number.ToString() + ")"

            Return False

        End Try

    End Function

    Public Function SUMA_CARDS_ActivacionCards(ByVal pVTID As String) As Boolean

        Try

            LimpiarVariables()

            Dim TmpRequest As WebRequest = System.Net.WebRequest.Create(URL_Cards)
            TmpRequest.Method = "POST"
            TmpRequest.ContentType = "application/json;charset=utf-8"

            Dim Data As New String("{ ""accion"":""activacionCards"", " +
            """vtId"":""" + pVTID + """ }")

            If DebugMode Then
                MsgBox(Data, MsgBoxStyle.Information, "DebugMode - Request")
            End If

            Dim Encoding As New System.Text.UTF8Encoding()
            Dim Bytes As Byte() = Encoding.GetBytes(Data)

            Using RequestStream As Stream = TmpRequest.GetRequestStream()
                'Send the data.
                RequestStream.Write(Bytes, 0, Bytes.Length)
            End Using

            Dim TmpResponse As WebResponse = TmpRequest.GetResponse

            If CType(TmpResponse, HttpWebResponse).StatusCode = HttpStatusCode.OK Then

                Dim DataStream As New StreamReader(TmpResponse.GetResponseStream)
                Dim Result As String = DataStream.ReadToEnd

                Dim Resp As JSON_Resul_Cards
                Resp = JsonConvert.DeserializeObject(Of JSON_Resul_Cards)(Result)

                Codigo = Resp.CodRespuesta
                Mensaje = Resp.mensajeRespuesta
                Voucher = Resp.nombreVoucher
                Referencia = Resp.numeroReferencia
                NumTarjeta = Resp.numeroTarjeta
                NumCedula = Resp.cedula
                MontoCompra = Resp.montoTransaccion
                FechaExp = Resp.fechaExpiracion
                HayCopiaVoucher = Resp.existeCopiaVoucher
                FechaTrans = Resp.fechaTransaccion
                HoraTrans = Resp.horaTransaccion
                TerminalV = Resp.terminalVirtual
                TipoTrans = Resp.tipoTransaccion
                NumAutorizacion = Resp.numeroAutorizacion
                CodigoDeAfiliacion = Resp.codigoAfiliacion
                TerminalID = Resp.tid
                NumRef = Resp.numeroReferencia
                NombreBancoAutorizador = Resp.nombreAutorizador
                CodigoBancoAdquiriente = Resp.codigoAdquiriente
                NumLote = Resp.numeroLote
                SaldoDisponible = Resp.saldoDisponible

                If Resp.CodRespuesta = "00" Then
                    Return True
                Else
                    Return False
                End If

            Else
                Err.Raise(CInt(CType(TmpResponse, HttpWebResponse).StatusCode), , "Error HTTP Status Failed")
                Return False
            End If

        Catch AnyEx As Exception

            'MsgBox(Err.Description, , "Compra Con Tarjetas VPosRest")

            Codigo = "-1"
            Mensaje = "Error al intentar establecer comunicación con el servicio (Cards). Información Adicional:" + vbNewLine +
            Err.Description(+" (" + Err.Number.ToString() + ")")

            Return False

        End Try

    End Function

    Public Function SUMA_CARDS_CambioPINCards(ByVal pVTID As String, _
    ByVal pCedula As String) As Boolean

        Try

            LimpiarVariables()

            Dim TmpRequest As WebRequest = System.Net.WebRequest.Create(URL_Cards)
            TmpRequest.Method = "POST"
            TmpRequest.ContentType = "application/json;charset=utf-8"

            Dim Data As New String("{ ""accion"":""cambioPINCards"", " +
            """vtId"":""" + pVTID + """, " +
            """cedula"":""" + pCedula + """ }")

            If DebugMode Then
                MsgBox(Data, MsgBoxStyle.Information, "DebugMode - Request")
            End If

            Dim Encoding As New System.Text.UTF8Encoding()
            Dim Bytes As Byte() = Encoding.GetBytes(Data)

            Using RequestStream As Stream = TmpRequest.GetRequestStream()
                'Send the data.
                RequestStream.Write(Bytes, 0, Bytes.Length)
            End Using

            Dim TmpResponse As WebResponse = TmpRequest.GetResponse

            If CType(TmpResponse, HttpWebResponse).StatusCode = HttpStatusCode.OK Then

                Dim DataStream As New StreamReader(TmpResponse.GetResponseStream)
                Dim Result As String = DataStream.ReadToEnd

                Dim Resp As JSON_Resul_Cards
                Resp = JsonConvert.DeserializeObject(Of JSON_Resul_Cards)(Result)

                Codigo = Resp.CodRespuesta
                Mensaje = Resp.mensajeRespuesta
                Voucher = Resp.nombreVoucher
                Referencia = Resp.numeroReferencia
                NumTarjeta = Resp.numeroTarjeta
                NumCedula = Resp.cedula
                MontoCompra = Resp.montoTransaccion
                FechaExp = Resp.fechaExpiracion
                HayCopiaVoucher = Resp.existeCopiaVoucher
                FechaTrans = Resp.fechaTransaccion
                HoraTrans = Resp.horaTransaccion
                TerminalV = Resp.terminalVirtual
                TipoTrans = Resp.tipoTransaccion
                NumAutorizacion = Resp.numeroAutorizacion
                CodigoDeAfiliacion = Resp.codigoAfiliacion
                TerminalID = Resp.tid
                NumRef = Resp.numeroReferencia
                NombreBancoAutorizador = Resp.nombreAutorizador
                CodigoBancoAdquiriente = Resp.codigoAdquiriente
                NumLote = Resp.numeroLote
                SaldoDisponible = Resp.saldoDisponible

                If Resp.CodRespuesta = "00" Then
                    Return True
                Else
                    Return False
                End If

            Else
                Err.Raise(CInt(CType(TmpResponse, HttpWebResponse).StatusCode), , "Error HTTP Status Failed")
                Return False
            End If

        Catch AnyEx As Exception

            'MsgBox(Err.Description, , "Compra Con Tarjetas VPosRest")

            Codigo = "-1"
            Mensaje = "Error al intentar establecer comunicación con el servicio (Cards). Información Adicional:" + vbNewLine +
            Err.Description + " (" + Err.Number.ToString() + ")"

            Return False

        End Try

    End Function

    Public Function SUMA_CARDS_MantenimientoPINTFisica(ByVal pCedula As String) As Boolean

        Try

            LimpiarVariables()

            Dim TmpRequest As WebRequest = System.Net.WebRequest.Create(URL_Cards)
            TmpRequest.Method = "POST"
            TmpRequest.ContentType = "application/json;charset=utf-8"

            Dim Data As New String("{ ""accion"":""mantenimientoPINTFisica"", " +
            """cedula"":""" + pCedula + """ }")

            If DebugMode Then
                MsgBox(Data, MsgBoxStyle.Information, "DebugMode - Request")
            End If

            Dim Encoding As New System.Text.UTF8Encoding()
            Dim Bytes As Byte() = Encoding.GetBytes(Data)

            Using RequestStream As Stream = TmpRequest.GetRequestStream()
                'Send the data.
                RequestStream.Write(Bytes, 0, Bytes.Length)
            End Using

            Dim TmpResponse As WebResponse = TmpRequest.GetResponse

            If CType(TmpResponse, HttpWebResponse).StatusCode = HttpStatusCode.OK Then

                Dim DataStream As New StreamReader(TmpResponse.GetResponseStream)
                Dim Result As String = DataStream.ReadToEnd

                Dim Resp As JSON_Resul_Cards
                Resp = JsonConvert.DeserializeObject(Of JSON_Resul_Cards)(Result)

                Codigo = Resp.CodRespuesta
                Mensaje = Resp.mensajeRespuesta
                Voucher = Resp.nombreVoucher
                Referencia = Resp.numeroReferencia
                NumTarjeta = Resp.numeroTarjeta
                NumCedula = Resp.cedula
                MontoCompra = Resp.montoTransaccion
                FechaExp = Resp.fechaExpiracion
                HayCopiaVoucher = Resp.existeCopiaVoucher
                FechaTrans = Resp.fechaTransaccion
                HoraTrans = Resp.horaTransaccion
                TerminalV = Resp.terminalVirtual
                TipoTrans = Resp.tipoTransaccion
                NumAutorizacion = Resp.numeroAutorizacion
                CodigoDeAfiliacion = Resp.codigoAfiliacion
                TerminalID = Resp.tid
                NumRef = Resp.numeroReferencia
                NombreBancoAutorizador = Resp.nombreAutorizador
                CodigoBancoAdquiriente = Resp.codigoAdquiriente
                NumLote = Resp.numeroLote
                SaldoDisponible = Resp.saldoDisponible

                If Resp.CodRespuesta = "00" Then
                    Return True
                Else
                    Return False
                End If

            Else
                Err.Raise(CInt(CType(TmpResponse, HttpWebResponse).StatusCode), , "Error HTTP Status Failed")
                Return False
            End If

        Catch AnyEx As Exception

            'MsgBox(Err.Description, , "Compra Con Tarjetas VPosRest")

            Codigo = "-1"
            Mensaje = "Error al intentar establecer comunicación con el servicio (Cards). Información Adicional:" + vbNewLine +
            Err.Description + " (" + Err.Number.ToString() + ")"

            Return False

        End Try

    End Function

    Public Function SUMA_CARDS_MantenimientoPINTVirtual(ByVal pCedula As String, _
    ByVal pNumeroTarjeta As String, _
    ByVal pFechaVencimiento As String) As Boolean

        Try

            LimpiarVariables()

            Dim TmpRequest As WebRequest = System.Net.WebRequest.Create(URL_Cards)
            TmpRequest.Method = "POST"
            TmpRequest.ContentType = "application/json;charset=utf-8"

            Dim Data As New String("{ ""accion"":""mantenimientoPINTVirtual"", " +
            """cedula"":""" + pCedula + """, " +
            """numeroTarjeta"":""" + pNumeroTarjeta + """, " +
            """fechaVcmto"":""" + pFechaVencimiento + """ }")

            If DebugMode Then
                MsgBox(Data, MsgBoxStyle.Information, "DebugMode - Request")
            End If

            Dim Encoding As New System.Text.UTF8Encoding()
            Dim Bytes As Byte() = Encoding.GetBytes(Data)

            Using RequestStream As Stream = TmpRequest.GetRequestStream()
                'Send the data.
                RequestStream.Write(Bytes, 0, Bytes.Length)
            End Using

            Dim TmpResponse As WebResponse = TmpRequest.GetResponse

            If CType(TmpResponse, HttpWebResponse).StatusCode = HttpStatusCode.OK Then

                Dim DataStream As New StreamReader(TmpResponse.GetResponseStream)
                Dim Result As String = DataStream.ReadToEnd

                Dim Resp As JSON_Resul_Cards
                Resp = JsonConvert.DeserializeObject(Of JSON_Resul_Cards)(Result)

                Codigo = Resp.CodRespuesta
                Mensaje = Resp.mensajeRespuesta
                Voucher = Resp.nombreVoucher
                Referencia = Resp.numeroReferencia
                NumTarjeta = Resp.numeroTarjeta
                NumCedula = Resp.cedula
                MontoCompra = Resp.montoTransaccion
                FechaExp = Resp.fechaExpiracion
                HayCopiaVoucher = Resp.existeCopiaVoucher
                FechaTrans = Resp.fechaTransaccion
                HoraTrans = Resp.horaTransaccion
                TerminalV = Resp.terminalVirtual
                TipoTrans = Resp.tipoTransaccion
                NumAutorizacion = Resp.numeroAutorizacion
                CodigoDeAfiliacion = Resp.codigoAfiliacion
                TerminalID = Resp.tid
                NumRef = Resp.numeroReferencia
                NombreBancoAutorizador = Resp.nombreAutorizador
                CodigoBancoAdquiriente = Resp.codigoAdquiriente
                NumLote = Resp.numeroLote
                SaldoDisponible = Resp.saldoDisponible

                If Resp.CodRespuesta = "00" Then
                    Return True
                Else
                    Return False
                End If

            Else
                Err.Raise(CInt(CType(TmpResponse, HttpWebResponse).StatusCode), , "Error HTTP Status Failed")
                Return False
            End If

        Catch AnyEx As Exception

            'MsgBox(Err.Description, , "Compra Con Tarjetas VPosRest")

            Codigo = "-1"
            Mensaje = "Error al intentar establecer comunicación con el servicio (Cards). Información Adicional:" + vbNewLine +
            Err.Description + " (" + Err.Number.ToString() + ")"

            Return False

        End Try

    End Function

    Private Sub LimpiarVariables()

        Try

            Codigo = String.Empty
            Mensaje = String.Empty
            Voucher = String.Empty
            Referencia = String.Empty
            NumTarjeta = String.Empty
            NumCedula = String.Empty
            MontoCompra = String.Empty
            MontoAvanceEfectivo = String.Empty
            MontoDeServicios = String.Empty
            MontoDeServiciosAprobado = String.Empty
            TipoCta = String.Empty
            TipoDeTarjeta = String.Empty
            FechaExp = String.Empty
            HayCopiaVoucher = String.Empty
            FechaTrans = String.Empty
            HoraTrans = String.Empty
            TerminalV = String.Empty
            TipoTrans = String.Empty
            NumAutorizacion = String.Empty
            CodigoDeAfiliacion = String.Empty
            TerminalID = String.Empty
            NumRef = String.Empty
            NombreBancoAutorizador = String.Empty
            CodigoBancoAdquiriente = String.Empty
            NumLote = String.Empty
            TipoProd = String.Empty
            BancoCheque = String.Empty
            NumCuenta = String.Empty
            NumCheque = String.Empty
            ArchivoCierre = String.Empty
            NumeroDeTarjetaEnmascarado = String.Empty
            SaldoDisponible = String.Empty

        Catch ex As Exception
            MsgBox(Err.Description, , "Limpiar Variables VPosRest")
        End Try

    End Sub

    Public Sub New()
        Try
            ServicePointManager.SecurityProtocol = (SecurityProtocolType.Tls Or 768 Or 3072 Or SecurityProtocolType.Ssl3)
        Catch
        End Try
        InitializePort(String.Empty)
    End Sub

    Public Function InitializePort(ByVal pPort As String)

        If String.IsNullOrEmpty(pPort) Then

            VPOS_Port = "8085" ' Default

        Else

            If DebugMode Then MsgBox(pPort, MsgBoxStyle.Information, "DebugMode - Port")

        End If

        Link = "http://localhost:" + VPOS_Port + "/vpos/metodo"
        URL_Cards = "http://localhost:" + VPOS_Port + "/vpos/metodo_cards"

        Return True

    End Function

End Class
